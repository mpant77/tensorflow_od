!/bin/bash

export PYTHONPATH=`pwd`/..:`pwd`/../slim

IN_PATH=/home/dsserver/projects/Manish/OD/processed-data/tf_records
OUT_PATH=/home/dsserver/projects/Manish/OD/processed-data/exported_graphs_fastercnn_40000
INPUT_TFRECORDS=$IN_PATH/pet_faces_val.record-00000-of-00064
OUTPUT_TFRECORDS=$OUT_PATH/detections.tfrecord
PATH_TO_GRAPH=$OUT_PATH/frozen_inference_graph.pb
LABEL_PATH=/home/dsserver/projects/Manish/OD/processed-data/object-detection.pbtxt

echo $INPUT_TFRECORDS, $OUTPUT_TFRECORDS, $PATH_TO_GRAPH

# python ../object_detection/inference/infer_detections_custom.py \
#     --input_tfrecord_paths=$INPUT_TFRECORDS \
#     --output_tfrecord_path=$OUTPUT_TFRECORDS \
#     --inference_graph=$PATH_TO_GRAPH

python confusion_matrix.py \
    --detections_record=$OUTPUT_TFRECORDS \
    --label_map=$LABEL_PATH \
    --output_path=$OUT_PATH/pr_metrics.csv \
    --cm_output_path=$OUT_PATH/confusion_matrix.csv