#!/bin/bash

export PYTHONPATH=`pwd`/..:`pwd`/../slim


CHECKPOINT_NUMBER=6003
#python object_detection/export_inference_graph.py \
#    --input_type image_tensor \
#    --pipeline_config_path ./allrooms/faster_rcnn_resnet101_allroom.config \
#    --trained_checkpoint_prefix ./allrooms/train_faster/model.ckpt-${CHECKPOINT_NUMBER} \
#    --output_directory ./allrooms/exported_graphs_faster

MODEL=fastercnn
BASE_MODEL_DIR=../common_${MODEL}_data
ROOT_DIR=/home/dsserver/projects/Manish/OD/processed-data
CONFIG_FILE=$ROOT_DIR/train_fastercnn/pipeline.config

python ../object_detection/export_inference_graph.py \
    --input_type image_tensor \
    --pipeline_config_path ${CONFIG_FILE} \
    --trained_checkpoint_prefix ${ROOT_DIR}/train_${MODEL}/model.ckpt-${CHECKPOINT_NUMBER} \
    --output_directory ${ROOT_DIR}/exported_graphs_${MODEL}_${CHECKPOINT_NUMBER}
