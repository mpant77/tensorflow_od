!/bin/bash

export PYTHONPATH=`pwd`/..:`pwd`/../slim

IN_PATH=/home/dsserver/projects/Manish/OD/processed-data/tf_records
OUT_PATH=/home/dsserver/projects/Manish/OD/processed-data/exported_graphs_fastercnn_46630
INPUT_TFRECORDS=$IN_PATH/pet_faces_val.record-00000-of-00064,$IN_PATH/pet_faces_val.record-00001-of-00064
OUTPUT_TFRECORDS=$OUT_PATH/detections.tfrecord
PATH_TO_GRAPH=$OUT_PATH/frozen_inference_graph.pb

echo $INPUT_TFRECORDS, $OUTPUT_TFRECORDS, $PATH_TO_GRAPH

python ../object_detection/inference/infer_detections.py \
    --input_tfrecord_paths=$INPUT_TFRECORDS \
    --output_tfrecord_path=$OUTPUT_TFRECORDS \
    --inference_graph=$PATH_TO_GRAPH