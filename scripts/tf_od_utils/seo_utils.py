def process(
        classes,
        scores,
        category_index,
        min_score_thresh=0.3):

    cabinet_style = {
        'raised-panel': 0,
        'recessed-panel': 0,
        'shaker': 0,
        'slab': 0,
        'glass-front': 0,
        'open': 0,
    }

    cabinet_finish = {
        'wood-dark': 0,
        'wood-medium': 0,
        'wood-light': 0,
        'wood-cherry': 0,
        'black': 0,
        'white': 0,
        'beige': 0,
        'grey': 0,
    }

    appliances_finish = {
        'stainless-steel': 0,
        'black': 0,
        'white': 0
    }

    appliances=['Refrigerator', 'Oven', 'Microwave', 'Dish Washer', 'Range']
    classes.shape = (-1, 1)
    scores.shape = (-1, 1)

    total_boxes = classes.shape[0]

    for index in range(total_boxes):
        score = scores[index][0]
        if score < min_score_thresh: continue
        class_index = classes[index][0]
        if class_index in category_index.keys():
            class_name = category_index[class_index]['name']
        else:
            class_name = 'N/A'
        major_class = class_name.split('.')[0]
        if major_class in appliances:
            appliances_color = class_name.split('.')[1]
            if appliances_color in appliances_finish:
                appliances_finish[appliances_color] = appliances_finish[appliances_color] + score

        if class_name.startswith('cabinet'):
            _, cab_style, cab_finish = class_name.split('.')
            if cab_style in cabinet_style:
                cabinet_style[cab_style] = (cabinet_style[cab_style] + score)
            if cab_finish in cabinet_finish:
                cabinet_finish[cab_finish] = (cabinet_finish[cab_finish] + score)

    seo_list = dict()
    seo_list['cabinet_style'] = []
    if cabinet_style['glass-front'] > 0.15:
        seo_list['cabinet_style'].append('glass-front')
        cabinet_style.pop('glass-front')
    if cabinet_style['open'] > 0.30:
        seo_list['cabinet_style'].append('open')
        cabinet_style.pop('open')
    cab_style = max(cabinet_style, key=cabinet_style.get)
    if cabinet_style[cab_style] > 0.25:
        seo_list['cabinet_style'].append(cab_style)

    cab_finish = max(cabinet_finish,key=cabinet_finish.get)
    if cabinet_finish[cab_finish] > 0.25:
        seo_list['cabinet_finish'] = cab_finish

    if len(seo_list['cabinet_style'])<0:
        seo_list.pop('cabinet_style')

    if appliances_finish['black'] > 0.15:
        seo_list['appliance_finish'] = 'black'
    elif appliances_finish['white'] > 0.15:
        seo_list['appliance_finish'] = 'white'
    elif appliances_finish['stainless-steel'] > 0.3:
        seo_list['appliance_finish'] = 'stainless-steel'

    return seo_list
