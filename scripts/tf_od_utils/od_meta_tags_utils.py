import time
import json
import numpy as np

converted_labels_dict = {
    'Ac Vent':'appliance.air-ventilation',
    'Air Hockey Table':'game.air-hockey-table',
    'Arcade Cabinet':'game.arcade-cabinet',
    'Basket':'decor.basket',
    'Bath Towel':'bath-accessory.bath-towel',
    'Bath Tub':'int.bathtub',
    'Bathtub Faucet':'faucet.bathtub-faucet',
    'Bean Bag':'furniture.bean-bag',
    'Bed Tray Table':'furniture.bed-tray-table',
    'Bedroom Bench':'furniture.bedroom-bench',
    'Bedside Table':'furniture.bedside-table',
    'Billiards Table':'game.pool-table',
    'Blinds':'window-treatment.blinds',
    'Bottle':'bar-wine.bottle',
    'Bowls':'tableware.bowl',
    'Cabinet.Glass-Front.Beige':'cabinet.glass-front.beige',
    'Cabinet.Glass-Front.Black':'cabinet.glass-front.black',
    'Cabinet.Glass-Front.Blue':'cabinet.glass-front.blue',
    'Cabinet.Glass-Front.Grey':'cabinet.glass-front.grey',
    'Cabinet.Glass-Front.Red':'cabinet.glass-front.red',
    'Cabinet.Glass-Front.White':'cabinet.glass-front.white',
    'Cabinet.Glass-Front.Wood-Cherry':'cabinet.glass-front.wood-cherry',
    'Cabinet.Glass-Front.Wood-Dark':'cabinet.glass-front.wood-dark',
    'Cabinet.Glass-Front.Wood-Light':'cabinet.glass-front.wood-light',
    'Cabinet.Glass-Front.Wood-Medium':'cabinet.glass-front.wood-medium',
    'Cabinet.Glass-Front.Yellow':'cabinet.glass-front.yellow',
    'Cabinet.Open.Beige':'cabinet.open.beige',
    'Cabinet.Open.Black':'cabinet.open.black',
    'Cabinet.Open.Blue':'cabinet.open.blue',
    'Cabinet.Open.Grey':'cabinet.open.grey',
    'Cabinet.Open.Red':'cabinet.open.bred',
    'Cabinet.Open.White':'cabinet.open.white',
    'Cabinet.Open.Wood-Dark':'cabinet.open.wood-dark',
    'Cabinet.Open.Wood-Light':'cabinet.open.wood-light',
    'Cabinet.Open.Wood-Medium':'cabinet.open.wood-medium',
    'Cabinet.Open.Yellow':'cabinet.open.yellow',
    'Cabinet.Raised-Panel.Beige':'cabinet.raised-panel.beige',
    'Cabinet.Raised-Panel.Black':'cabinet.raised-panel.black',
    'Cabinet.Raised-Panel.Blue':'cabinet.raised-panel.blue',
    'Cabinet.Raised-Panel.Green':'cabinet.raised-panel.green',
    'Cabinet.Raised-Panel.Grey':'cabinet.raised-panel.grey',
    'Cabinet.Raised-Panel.Olive':'cabinet.raised-panel.olive',
    'Cabinet.Raised-Panel.Red':'cabinet.raised-panel.red',
    'Cabinet.Raised-Panel.White':'cabinet.raised-panel.white',
    'Cabinet.Raised-Panel.Wood-Cherry':'cabinet.raised-panel.wood-cherry',
    'Cabinet.Raised-Panel.Wood-Dark':'cabinet.raised-panel.wood-dark',
    'Cabinet.Raised-Panel.Wood-Light':'cabinet.raised-panel.wood-light',
    'Cabinet.Raised-Panel.Wood-Medium':'cabinet.raised-panel.wood-medium',
    'Cabinet.Raised-Panel.Yellow':'cabinet.raised-panel.yellow',
    'Cabinet.Recessed-Panel.Beige':'cabinet.recessed-panel.beige',
    'Cabinet.Recessed-Panel.Black':'cabinet.recessed-panel.black',
    'Cabinet.Recessed-Panel.Blue':'cabinet.recessed-panel.blue',
    'Cabinet.Recessed-Panel.Grey':'cabinet.recessed-panel.grey',
    'Cabinet.Recessed-Panel.Olive':'cabinet.recessed-panel.olive',
    'Cabinet.Recessed-Panel.Red':'cabinet.recessed-panel.red',
    'Cabinet.Recessed-Panel.White':'cabinet.recessed-panel.white',
    'Cabinet.Recessed-Panel.Wood-Cherry':'cabinet.recessed-panel.wood-cherry',
    'Cabinet.Recessed-Panel.Wood-Dark':'cabinet.recessed-panel.wood-dark',
    'Cabinet.Recessed-Panel.Wood-Light':'cabinet.recessed-panel.wood-light',
    'Cabinet.Recessed-Panel.Wood-Medium':'cabinet.recessed-panel.wood-medium',
    'Cabinet.Recessed-Panel.Yellow':'cabinet.recessed-panel.yellow',
    'Cabinet.Shaker.Beige':'cabinet.shaker.beige',
    'Cabinet.Shaker.Black':'cabinet.shaker.black',
    'Cabinet.Shaker.Blue':'cabinet.shaker.blue',
    'Cabinet.Shaker.Grey':'cabinet.shaker.grey',
    'Cabinet.Shaker.Olive':'cabinet.shaker.olive',
    'Cabinet.Shaker.Red':'cabinet.shaker.red',
    'Cabinet.Shaker.White':'cabinet.shaker.white',
    'Cabinet.Shaker.Wood-Cherry':'cabinet.shaker.wood-cherry',
    'Cabinet.Shaker.Wood-Dark':'cabinet.shaker.wood-dark',
    'Cabinet.Shaker.Wood-Light':'cabinet.shaker.wood-light',
    'Cabinet.Shaker.Wood-Medium':'cabinet.shaker.wood-medium',
    'Cabinet.Shaker.Yellow':'cabinet.slab.olive',
    'Cabinet.Slab.Beige':'cabinet.slab.beige',
    'Cabinet.Slab.Black':'cabinet.slab.black',
    'Cabinet.Slab.Blue':'cabinet.slab.blue',
    'Cabinet.Slab.Brown':'cabinet.slab.brown',
    'Cabinet.Slab.Grey':'cabinet.slab.grey',
    'Cabinet.Slab.Red':'cabinet.slab.red',
    'Cabinet.Slab.White':'cabinet.slab.white',
    'Cabinet.Slab.Wood-Dark':'cabinet.slab.wood-dark',
    'Cabinet.Slab.Wood-Light':'cabinet.slab.wood-light',
    'Cabinet.Slab.Wood-Medium':'cabinet.slab.wood-medium',
    'Cabinet.Slab.Yellow':'cabinet.slab.yellow',
    'Candle Stand':'decor.candle-holder',
    'Casino Table':'game.casino-table',
    'Ceiling Fan':'electrical.ceiling-fan.ceiling-fan-light',
    'Ceiling Fan Light':'electrical.ceiling-fan.ceiling-fan',
    'Ceiling Mounted Light':'lighting.ceiling-light',
    'Ceiling Speakers':'electrical.ceiling-speaker',
    'Ceiling Spotlight':'lighting.ceiling-spotlight',
    'Center Table':'furniture.center-table',
    'Chair':'furniture.chair.chair',
    'Chaise Lounge':'furniture.chaise-lounge',
    'Chandelier':'lighting.chandelier',
    'Chimney Outlet':'ext.chimney',
    'Chopping Board':'tableware.cutting-board',
    'Clock':'decor.clock',
    'Coffee Maker':'appliance.coffee-maker',
    'Console Table':'furniture.table.console-table',
    'Cooktop':'appliance.cook-top',
    'Countertop.Granite':'int.countertop.granite',
    'Countertop.Laminate':'int.countertop.laminate',
    'Countertop.Marble':'int.countertop.marble',
    'Countertop.Plain':'int.countertop.plain',
    'Countertop.Wood':'int.countertop.wood',
    'Cups':'tableware.cup',
    'Curtains':'window-treatment.curtain-drape',
    'Dartboard':'game.dartboard',
    'Decorative Item':'decor.decorative-item',
    'Decorative Wall Items':'decor.wall-decor',
    'Desk':'int.desk',
    'Dining Table':'furniture.table.dining-table',
    'Dish Washer.Black':'appliance.dishwasher.black',
    'Dish Washer.Coloured':'appliance.dishwasher.colored',
    'Dish Washer.Stainless-Steel':'appliance.dishwasher.stainless-steel',
    'Dish Washer.White':'appliance.dishwasher.white',
    'Double Bed':'furniture.bed.double-bed',
    'Drawers':'furniture.drawers',
    'Dressing Table':'furniture.dressing-table',
    'Faucet':'faucet.kitchen-faucet',
    'Fireplace Outlet':'ext.chimney',
    'Fireplace':'int.fireplace',
    'Floor Lamp':'lamp.floor',
    'Floor.Carpet.Beige':'floor.carpet.beige',
    'Floor.Carpet.Black':'floor.carpet.black',
    'Floor.Carpet.Blue':'floor.carpet.blue',
    'Floor.Carpet.Brown':'floor.carpet.brown',
    'Floor.Carpet.Gray':'floor.carpet.gray',
    'Floor.Carpet.Patterned':'floor.carpet.patterned',
    'Floor.Cement':'floor.cement.gray',
    'Floor.Tile.Cement':'floor.tile.cement',
    'Floor.Tile.Ceramic':'floor.tile.ceramic',
    'Floor.Tile.Limestone':'floor.tile.limestone',
    'Floor.Tile.Marble':'floor.tile.marble',
    'Floor.Tile.Mosaic':'floor.tile.mosaic',
    'Floor.Tile.Stone':'floor.tile.stone',
    'Floor.Wooden.Dark':'floor.wooden.dark',
    'Floor.Wooden.Light':'floor.wooden.light',
    'Floor.Wooden.Medium':'floor.wooden.medium',
    'Foosball Table':'game.fusball-table',
    'Four Poster Bed':'furniture.bed.four-poster-bed',
    'Fruit Veg Tray':'tableware.fruit-veg-tray',
    'Glass Door':'door.glass',
    'Glass':'bar-wine.wine-glass',
    'Guitar':'game.guitar',
    'Hand Shower':'shower.hand-shower',
    'Jars':'tableware.jar',
    'Jug':'tableware.jug',
    'Juicer Blender':'appliance.juicer-blender',
    'Kettle':'cookware.kettle',
    'Keyboard':'game.keyboard',
    'Kitchen Island':'int.kitchen-island',
    'Laptop':'electrical.laptop',
    'Microwave.Black':'appliance.microwave.black',
    'Microwave.Coloured':'appliance.microwave.colored',
    'Microwave.Stainless-Steel':'appliance.microwave.stainless-steel',
    'Microwave.White':'appliance.microwave.white',
    'Mirror':'decor.mirror',
    'Mixer Tap':'faucet.mixer-tap',
    'Ottoman':'furniture.ottoman',
    'Oven.Black':'appliance.oven.black',
    'Oven.Coloured':'appliance.oven.colored',
    'Oven.Stainless-steel':'appliance.oven.stainless-steel',
    'Oven.White':'appliance.oven.white',
    'Painting':'decor.painting',
    'Papasan Chair':'furniture.chair.papasan-chair',
    'Pendant Light':'lighting.pendent',
    'Pendent Light':'lighting.pendent',
    'Photo Frame':'decor.photo-frame',
    'Piano':'game.piano',
    'Pillow':'bedding.pillow',
    'Planter':'decor.planter',
    'Plates':'tableware.plate',
    'Pond':'ext.pond',
    'Poster':'decor.poster',
    'Projector Cabinet':'int.tv-stand',
    'Projector Screen':'electrical.projector-screen',
    'Projector':'electrical.projector',
    'Range.Black':'appliance.range.black',
    'Range.Coloured':'appliance.range.colored',
    'Range.Stainless-steel':'appliance.range.stainless-steel',
    'Range.White':'appliance.range.white',
    'Recessed Light':'lighting.recessed',
    'Recliner Sofa':'sofa.recliner-sofa',
    'Refrigerator.Black':'appliance.refrigerator.black',
    'Refrigerator.Coloured':'appliance.refrigerator.colored',
    'Refrigerator.Stainless-Steel':'appliance.refrigerator.stainless-steel',
    'Refrigerator.White':'appliance.refrigerator.white',
    'Round Stone Fire Pit':'ext.fire-pit',
    'Sectional Sofa':'sofa.sectional-sofa',
    'Shelves':'furniture.shelf',
    'Shower Area':'int.shower',
    'Shower Cum Tub':'shower.shower-bathtub-combination',
    'Shower Head':'shower.shower-head',
    'Shower Mixer':'shower.shower-mixer',
    'Shuffleboard':'game.shuffle-board',
    'Side Table':'furniture.side-table',
    'Sidelite':'door.sidelites',
    'Single Bed':'furniture.bed.single-bed',
    'Bathroom Sink':'int.sink',
    'Kitchen Sink':'int.sink',
    'Skee-Ball Machine':'game.skee-ball-machine',
    'Soap Dish':'bath-accessory.soap-dish',
    'Soap Dispenser':'bath-accessory.soap-dispenser',
    'Sofa Bench':'furniture.sofa-bench',
    'Sofa Blanket':'bedding.sofa-blanket',
    'Sofa Blankets':'bedding.sofa-blanket',
    'Sofa Double':'furniture.sofa',
    'Sofa Four':'furniture.sofa',
    'Sofa Single':'furniture.sofa',
    'Sofa Triple':'furniture.sofa',
    'Speakers':'electrical.speaker',
    'Stools':'furniture.stool',
    'Study Lamp':'lamp.desk',
    'Table Clock':'decor.table-clock',
    'Table Lamp':'lamp.table',
    'Table Mat':'decor.table-mat',
    'Tableware':'tableware.crockery-set',
    'Telephone':'appliance.telephone',
    'Tictactoe Board':'game.tictactoe-board',
    'Toaster':'appliance.toaster',
    'Toddler Bed':'furniture.bed.toddler-bed',
    'Toothbrush Holder':'bath-accessory.toothbrush-holder',
    'Towel Holder':'bath-accessory.towel-holder',
    'Toys':'decor.toys',
    'Tray':'tableware.serving-tray',
    'Trellis':'ext.trellises',
    'Trophy':'decor.trophy',
    'TT Table':'game.table-tennis',
    'TV Cabinet':'furniture.entertainment-center',
    'TV Unit':'int.tv-stand',
    'TV':'electrical.tv',
    'Valance':'window-treatment.valance',
    'Vase':'decor.vase',
    'Vent Hood':'appliance.vent-hood',
    'Wall Lights':'lighting.wall-mounted',
    'Wall Shelf':'furniture.wall-shelf',
    'Wall Speakers':'electrical.wall-speaker',
    'Wardrobe':'furniture.wardrobe',
    'Water Dispenser':'appliance.water-dispenser',
    'Toilet':'int.toilet',
    'Window':'int.window',
    'Wine Holder':'bar-wine.wine-holder',
    'Wine Rack':'bar-wine.wine-rack',
    'Wooden Door':'door.wood'

}


def get_meta_tag_from_od(classes, scores, category_index, min_score_thresh=0.3):
    """
    Identify meta tags to describe the details of image

        scores:
        category_index_list: a list containing category names.
    :param classes: a numpy array of shape [N]. Note that class indices are 1-based,
            and match the keys in the label map.
    :param scores: a numpy array of shape [N] or None.  If scores=None, then
          this function assumes that the boxes to be plotted are groundtruth
          boxes and plot all boxes as black with no classes or scores.
    :param labels_list: a list containing category names.
    :param min_score_thresh:
    :return:
    """

    #print(classes)
    cabinet_style = {
        'raised-panel': 0,
        'recessed-panel': 0,
        'shaker': 0,
        'slab': 0,
        'glass-front': 0,
        'open': 0,
    }

    cabinet_finish = {
        'wood-dark': 0,
        'wood-medium': 0,
        'wood-light': 0,
        'wood-cherry': 0,
        'black': 0,
        'white': 0,
        'beige': 0,
        'grey': 0,
    }

    floor_style = {
        'carpet.beige': 0,
        'carpet.brown': 0,
        'carpet.gray': 0,
        'carpet.patterned': 0,
        'tile.ceramic': 0,
        'tile.limestone': 0,
        'tile.marble': 0,
        'tile.stone': 0,
        'wooden.dark': 0,
        'wooden.light': 0,
        'wooden.medium': 0,
    }

    appliances_finish = {
        'stainless-steel': 0,
        'black': 0,
        'white': 0
    }

    countertop_type = {
        'granite': 0,
        'marble': 0,
        'plain': 0,
        'laminate': 0,
        'wooden': 0,
    }

    appliances = ['Refrigerator', 'Oven', 'Microwave', 'Dish Washer', 'Range']
    classes.shape = (-1, 1)
    scores.shape = (-1, 1)

    total_boxes = classes.shape[0]
    merged_output_dict = {}
    for index in range(total_boxes):
        score = scores[index][0]
        if score < min_score_thresh: continue
        class_index = int(classes[index][0])
        #class_name = labels_list[class_index - 1]
        if class_index in category_index.keys():
            class_name = category_index[class_index]['name']
        else:
            class_name = 'N/A'
        #print(class_name)
        if class_name in converted_labels_dict.keys():
            class_name_new = converted_labels_dict[class_name]
            tokens = class_name_new.split('.')
            main_class = tokens[0]
            sub_class = ".".join(tokens[1:3])
            if class_name_new.startswith('cabinet'):
                _, cab_style, cab_finish = class_name_new.split('.')
                if cab_style in cabinet_style:
                    cabinet_style[cab_style] = (cabinet_style[cab_style] + score)
                if cab_finish in cabinet_finish:
                    cabinet_finish[cab_finish] = (cabinet_finish[cab_finish] + score)
            else:
                if not main_class in merged_output_dict.keys():
                    merged_output_dict[main_class] = []
                if len(tokens) == 3:
                    sub_class_temp, class_type = tokens[1:3]
                    items_in_class = merged_output_dict[main_class]
                    already_present = False
                    for n in items_in_class:
                        if n.split('.')[0] == sub_class_temp:
                            already_present = True
                            break
                    if already_present: continue
                    if sub_class not in merged_output_dict[main_class]:
                        merged_output_dict[main_class].append(sub_class)
                elif len(tokens) == 2:
                    if sub_class not in merged_output_dict[main_class]:
                        merged_output_dict[main_class].append(sub_class)
    merged_output_dict['cabinet_style'] = []
    if cabinet_style['glass-front'] > 0.15:
        merged_output_dict['cabinet_style'].append('glass-front')
        cabinet_style.pop('glass-front')
    if cabinet_style['open'] > 0.30:
        merged_output_dict['cabinet_style'].append('open')
        cabinet_style.pop('open')
    cab_style = max(cabinet_style, key=cabinet_style.get)
    if cabinet_style[cab_style] > 0.25:
        merged_output_dict['cabinet_style'].append(cab_style)

    cab_finish = max(cabinet_finish, key=cabinet_finish.get)
    if cabinet_finish[cab_finish] > 0.25:
        merged_output_dict['cabinet_finish'] = [cab_finish]

    if len(merged_output_dict['cabinet_style']) == 0:
        merged_output_dict.pop('cabinet_style')

    return merged_output_dict

