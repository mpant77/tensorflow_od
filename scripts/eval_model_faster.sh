#!/bin/bash
export CUDA_VISIBLE_DEVICES=""
export PYTHONPATH=`pwd`/..:`pwd`/../object_detection:`pwd`/../slim
echo $PYTHONPATH

ROOT_DIR=/home/mpant/OD/processed-data
CONFIG_FILE=$ROOT_DIR/train_fastercnn/pipeline.config
SAVE_PR_DIRECTORY=$ROOT_DIR/rc_$MODEL

nohup python -u ../object_detection/eval.py --checkpoint_dir=$ROOT_DIR/train_fastercnn --eval_dir=$ROOT_DIR/eval_faster --save_pr_dir=$SAVE_PR_DIRECTORY  --pipeline_config_path=$CONFIG_FILE --logtostderr 2>&1  >>log_eval.txt &

tail -f log_eval.txt