#!/bin/bash

export PYTHONPATH=`pwd`/..:`pwd`/../slim
MODEL=fastercnn
#MODEL=ssd
BASE_MODEL_DIR=../common_${MODEL}_data
ROOT_DIR=/home/dstest/kashif/OD/allrooms
CONFIG_FILE=$BASE_MODEL_DIR/pipeline.config
#CONFIG_FILE=$ROOT_DIR/faster_rcnn_resnet101_allroom.config
#CONFIG_FILE=$ROOT_DIR/rfcn_resnet101_greatroom.config
LOG_FILE=log_train_${MODEL}.txt

nohup python -u ../object_detection/model_main.py --model_dir=$ROOT_DIR/model_${MODEL} --pipeline_config_path=$CONFIG_FILE --logtostderr 2>&1 >>$LOG_FILE &
#nohup python -u ../object_detection/train.py --train_dir=$ROOT_DIR/train_rfcn --pipeline_config_path=$CONFIG_FILE --logtostderr 2>&1 >>$LOG_FILE &

tail -f $LOG_FILE
