#!/bin/bash

vars=`find ../processed-data/train_fastercnn/ -name "model.ckpt-*.index" | cut -d '-' -f 3 | cut -d '.' -f 1`
echo "Total ckpts "
echo "$vars"
for var in $vars
do
if (($var >= 10000 && $var <= 300000)); then
  # your code
echo "ckpt in range -> $var"
sed -i "1c\model_checkpoint_path: \"/home/dsserver/projects/Manish/OD/processed-data/train_fastercnn/model.ckpt-$var\"" /home/dsserver/projects/Manish/OD/processed-data/train_fastercnn/checkpoint

$(/home/dsserver/projects/Manish/OD/scripts/eval_model.sh > /home/dsserver/projects/Manish/OD/processed-data/model_$var.txt)
echo "Ran eval script for $var"
fi
done
