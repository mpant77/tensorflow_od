#!/bin/bash

export PYTHONPATH=`pwd`/..:`pwd`/../slim

MODEL=ssd
#BASE_MODEL_DIR=../common_${MODEL}_data
ROOT_DIR=/home/dsserver/projects/Manish/OD/processed-data
CONFIG_FILE=$ROOT_DIR/train_${MODEL}/pipeline.config
SAVE_PR_DIRECTORY=$ROOT_DIR/rc_$MODEL
#ROOT_DIR=/home/dstest/kashif/OD/allrooms
#CONFIG_FILE=$ROOT_DIR/rfcn_resnet101_greatroom.config
#CONFIG_FILE=$ROOT_DIR/faster_rcnn_resnet101_allroom.config


nohup python -u ../object_detection/eval.py --checkpoint_dir=$ROOT_DIR/train_ssd --eval_dir=$ROOT_DIR/eval_$MODEL  --pipeline_config_path=$CONFIG_FILE --save_pr_dir=$SAVE_PR_DIRECTORY >>log_eval_allroooms.txt &
#python ../object_detection/eval.py --checkpoint_dir=$ROOT_DIR/train_$MODEL --eval_dir=$ROOT_DIR/eval_$MODEL  --pipeline_config_path=$CONFIG_FILE --save_pr_dir=$SAVE_PR_DIRECTORY

tail -f log_eval_allroooms.txt
