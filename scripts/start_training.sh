#!/bin/bash

export PYTHONPATH=`pwd`/..:`pwd`/../slim
MODEL=fastercnn
#MODEL=ssd_v1_fpn
BASE_MODEL_DIR=../common_${MODEL}_data
ROOT_DIR=/home/dsserver/projects/Manish/OD/processed-data
CONFIG_FILE=$BASE_MODEL_DIR/pipeline.config
LOG_FILE=log_train_${MODEL}.txt

nohup python -u ../object_detection/train.py --train_dir=$ROOT_DIR/train_${MODEL} --pipeline_config_path=$CONFIG_FILE --logtostderr 2>&1 >>$LOG_FILE &
#nohup python -u ../object_detection/train.py --train_dir=$ROOT_DIR/train_rfcn --pipeline_config_path=$CONFIG_FILE --logtostderr 2>&1 >>$LOG_FILE &

tail -f $LOG_FILE
