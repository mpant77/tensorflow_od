import os
import cv2
import pandas as pd
import numpy as np
import tensorflow as tf
import xml.etree.ElementTree as ET
from collections import OrderedDict


PATH_TO_IMAGES = '/home/krjmeer/Work/OD/allrooms_data_14_test/images'
PATH_TO_GT_LABELS = '/home/krjmeer/Work/OD/Training Data/xmls_copy_test'
PATH_TO_LABELS_FILE = '/home/krjmeer/Work/OD/Training Data/data_main/labels.txt'
# PATH_TO_FROZEN_GRAPH = '/home/krjmeer/Work/home_vision_cache/tf_od_graph_faster/frozen_inference_graph.pb'
PATH_TO_FROZEN_GRAPH = '/home/krjmeer/exported_graphs_fastercnn_401670/frozen_inference_graph.pb'
# PATH_TO_FROZEN_GRAPH = '/home/krjmeer/exported_graphs_rfcn_384131/frozen_inference_graph.pb'


# PATH_TO_LABELS_FILE = '/home/krjmeer/Work/OD/Training Data/data_main/labels_343.txt'
# PATH_TO_FROZEN_GRAPH = '/home/krjmeer/latest_od_model/frozen_inference_graph.pb'


def get_labels_per_line(file_path):
    with open(file_path) as f:
        labels = f.readlines()
        labels = [l.rstrip().lower() for l in labels]
        return labels


def load_image_into_numpy_array(image_path):
    """Return a 3-D image numpy array and shape

    Args:
        image_path: path to the image

    Returns:
        3D image numpy array od shape (height, width, 3) dtype: uint8
    """
    img = cv2.imread(image_path)
    print(image_path)
    image_np = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    return image_np


def evaluate_single(gt_labels_list, classes, scores, score_thresholds, pr_matrix_dict):
    for score_threshold in score_thresholds:
        classes_scr = classes[scores>score_threshold]
        classes_scr = set(classes_scr)
        PR_Matrix = pr_matrix_dict[score_threshold]
        for i in range(PR_Matrix.shape[0]):
            if i in gt_labels_list:
                if i in classes_scr:
                    PR_Matrix[i][0] += 1
                else:
                    PR_Matrix[i][1] += 1
            else:
                if i in classes_scr:
                    PR_Matrix[i][2] += 1
                else:
                    PR_Matrix[i][3] += 1
        pr_matrix_dict[score_threshold] = PR_Matrix
    return pr_matrix_dict


def pr_to_csv(pr_numpy, labels, out_file_path):
    df = pd.DataFrame(pr_numpy,columns=['True Positives', 'False Negatives', 'False Positives', 'True Negatives'])
    total_occurances =  df['True Positives'] + df['False Negatives']
    df.insert(0, "Total Occurences",total_occurances, True)
    recall = (df['True Positives']*100.0) / (total_occurances + 1e-12)
    precision = (df['True Positives'] * 100.0) / (df['True Positives'] + df['False Positives'] + 1e-12)
    f1_score =  2 * (precision * recall) / ( precision + recall + 1e-12)
    df.insert(df.shape[1], "Recall", recall, True)
    df.insert(df.shape[1], "Precision", precision, True)
    df.insert(df.shape[1], "F1 Score", f1_score, True)
    df.insert(0, "Class Name", labels, True)
    df.to_csv(out_file_path,index=False)
    return f1_score, total_occurances


def get_labels_from_xml(path_to_gt_label):
    obj_list = []
    tree = ET.parse(path_to_gt_label)
    xml_root = tree.getroot()
    for ele in xml_root.findall('object'):
        obj_name = ele.find('name')
        obj_list.append(obj_name.text.replace('multi-label.','').lower())
    return list(dict.fromkeys(obj_list))


def save_f1_summary(labels, f1_score_dict, pr_matrix_dict, total):
    df = pd.DataFrame(labels)
    df.insert(df.shape[1], "Total Occurences", total, True)
    for score,f1_scores in f1_score_dict.items():
        df.insert(df.shape[1], 'f1@_' + str(score), f1_scores, True)

    f1_numpy = np.array(list(f1_score_dict.values()))
    pr_numpy = np.array(list(pr_matrix_dict.values()))
    all_scores_threshold = list(f1_score_dict.keys())
    best_f1_score_index = np.argmax(f1_numpy, axis=0)
    max_f1 = []
    tp = []
    fn = []
    fp = []
    tn = []
    best_score_threshold = []
    for i in range(len(best_f1_score_index)):
        max_f1.append(f1_numpy[best_f1_score_index[i],i])
        tp.append(pr_numpy[best_f1_score_index[i], i, 0])
        fn.append(pr_numpy[best_f1_score_index[i], i, 1])
        fp.append(pr_numpy[best_f1_score_index[i], i, 2])
        tn.append(pr_numpy[best_f1_score_index[i], i, 3])
        best_score_threshold.append(all_scores_threshold[best_f1_score_index[i]])
    df.insert(2, 'Best Score Threshold', best_score_threshold, True)
    df.insert(3, 'Max F1', max_f1, True)
    df.insert(4, 'True Positives', tp, True)
    df.insert(5, 'False Negatives', fn, True)
    df.insert(6, 'False Positive', fp, True)
    df.insert(7, 'True Negatives', tn, True)
    df.to_csv('f1_summary.csv')




def save_summaries(pr_matrix_dict, labels):
    f1_score_dict = OrderedDict()
    for score_threshold,pr_matrix in pr_matrix_dict.items():
        f1_score, total = pr_to_csv(pr_matrix, labels, 'pr_' + str(score_threshold) + '.csv')
        f1_score_dict[score_threshold] = f1_score
    save_f1_summary(labels, f1_score_dict, pr_matrix_dict, total)

def evaluate(images_dir, gt_labels_dir, labels_file, frozen_pb_graph,
             score_thresholds=[0.5], from_xml=True):
    """

    :param images_dir:
    :param gt_labels_dir:
    :param labels_file:
    :param Class_name:
    :param score_threshold:
    :return:
    """

    def load_graph(path_to_pb_file):
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(path_to_pb_file, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
                print("Model Successfully loaded from", path_to_pb_file)
                config = tf.ConfigProto(allow_soft_placement=True)
                config.gpu_options.per_process_gpu_memory_fraction = 0.5
                sess = tf.Session(config=config, graph=detection_graph)
                tensor_dict = {}
                for key in [
                    'num_detections', 'detection_boxes', 'detection_scores',
                    'detection_classes'
                ]:
                    tensor_name = key + ':0'
                    tensor_dict[key] = detection_graph.get_tensor_by_name(
                        tensor_name)
                image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
                graph = detection_graph
                tensor_dict = tensor_dict
                return image_tensor, tensor_dict, sess, graph

    image_tensor, tensor_dict, sess, graph = load_graph(frozen_pb_graph)
    labels = get_labels_per_line(labels_file)
    labels.insert(0, 'Background')
    total_labels = len(labels)
    images_files_list = os.listdir(images_dir)

    # gt_labels_files_list = os.listdir(gt_labels_dir)
    pr_matrix_dict = OrderedDict()
    for score_threshold in score_thresholds:
        PR_Matrix = np.zeros((total_labels, 4))
        pr_matrix_dict[score_threshold] = PR_Matrix

    for image_name in images_files_list:
        path_to_image = os.path.join(images_dir, image_name)
        image_file_base_name = '.'.join(image_name.split(".")[:-1])
        path_to_gt_label = os.path.join(gt_labels_dir,image_file_base_name + '.xml')
        if os.path.exists(path_to_gt_label):
            image_np = load_image_into_numpy_array(path_to_image)
            if from_xml:
                gt_labels_list = get_labels_from_xml(path_to_gt_label)
            else:
                gt_labels_list = get_labels_per_line(path_to_gt_label)

            for i,j in enumerate(gt_labels_list):
                try:
                    gt_labels_list[i] = labels.index(j)
                except Exception as e:
                    print(e)
                    continue
            output_dict = sess.run(tensor_dict,
                                   feed_dict={image_tensor: np.expand_dims(image_np, 0)})
            pr_matrix_dict = evaluate_single(gt_labels_list,
                            output_dict['detection_classes'],
                            output_dict['detection_scores'],
                            score_thresholds,
                            pr_matrix_dict)


                # print(output_dict)
        # pr_to_csv(PR_Matrix, labels, 'pr_' + str(score_threshold) + '.csv')
    save_summaries(pr_matrix_dict,labels)


evaluate(PATH_TO_IMAGES,
         PATH_TO_GT_LABELS,
         PATH_TO_LABELS_FILE,
         PATH_TO_FROZEN_GRAPH,
         # score_thresholds=[0.5])
         # score_thresholds=np.arange(0,1.1,0.1))
         score_thresholds=[0.1,0.3,0.5,0.6,0.8])