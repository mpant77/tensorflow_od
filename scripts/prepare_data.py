import os 
import shutil
import pandas as pd
import xml.etree.ElementTree as ET

from PIL import Image

ROOT_PATH = '/home/dsserver/projects/Manish/OD/raw-data'   ##Root Directory Path
SUB_XML_PATH = ROOT_PATH     ##Path to xml root folder. There can be any folders inside  
SUB_IMAGES_PATH = ROOT_PATH     ## Path to images root folder
labels_file_path = ROOT_PATH+'/labels.txt'               # Text Files containing labels names every new line. This will be used to generate pbtxt file

OUTPUT_ROOT = '/home/dsserver/projects/Manish/OD/processed-data'  ##Output main folder
OUTPUT_IMAGES_PATH = OUTPUT_ROOT + '/images'          ## directory to copy all images for the respective image files
OUTPUT_XML_PATH = OUTPUT_ROOT + '/annotations/xmls'   ## directory to copy all images for the respective xml files
CSV_SAVE_PATH = OUTPUT_ROOT+'/data.csv'               ## Bounding Box info csv files  
CSV_SUMMARY_SAVE_PATH = OUTPUT_ROOT+'/data_summary.csv'   ## Totals Objects Summary  csv
output_pbtxt_path = OUTPUT_ROOT + '/object-detection.pbtxt'  ## Generated pftxt file from labels file.

TRAINVAL_FILE = OUTPUT_ROOT+'/annotations/trainval.txt'    ##List of Files without files extension to be used for train and validation task
TEST_FILE = OUTPUT_ROOT+'/annotations/test.txt'            ##List of Files without files extension to be used for test task




TRAIN_RATIO = 0.96     #Ratio for defining Trainval  and test examples
MAX_WIDTH = 1000
MAX_HEIGHT = 1000
"""
Create All Folders If not Already

"""

if( not os.path.exists(OUTPUT_ROOT)):
    os.makedirs(OUTPUT_ROOT)
if( not os.path.exists(OUTPUT_XML_PATH)):
    os.makedirs(OUTPUT_XML_PATH)
if( not os.path.exists(OUTPUT_IMAGES_PATH)):
    os.makedirs(OUTPUT_IMAGES_PATH)
   

 
"""
Convert lables.txt file to pbtxt format

"""

with open(labels_file_path,'r') as f:
  labels = [l.rstrip() for l in f.readlines()]

with open(output_pbtxt_path,'w') as f:
    for i,l in enumerate(labels):
        f.write('item {\n  id: '+str(i+1)+'\n  name: '+'\''+l+'\''+'\n}\n\n')

"""
Accumulate all xml files under any folder inside root folder.
extract xml data and prepare csv file

"""
##Copying xmls files 
xml_data=[]
for root,dirs,files in os.walk(SUB_XML_PATH):
    for xml_file in files:
        if(xml_file.endswith('xml')):
            # print(root,xml_file)
            #shutil.copy(os.path.join(root,xml_file),os.path.join(OUTPUT_XML_PATH,xml_file))
            xmlnode=ET.parse(os.path.join(root,xml_file))
            xml_root=xmlnode.getroot()
            size_ele=xml_root.findall('size')[0]
            width_node=size_ele.find('width')
            #print (width.text,xml_file)
            height_node=size_ele.find('height')
            width = int(width_node.text)
            height = int(height_node.text)
            if (not width==0) or (not height==0):
                if (width>height):
                    wpercent = (MAX_WIDTH/float(width))
                    if (wpercent<1):
                        width = MAX_WIDTH
                        height = int((float(height)*float(wpercent)))
                    else:
                        wpercent = 1
                else:
                    wpercent = (MAX_HEIGHT/float(height))
                    if (wpercent<1):
                        height = MAX_HEIGHT
                        width = int((float(width)*float(wpercent)))
                    else:
                        wpercent = 1
            else:
                continue
            width_node.text=str(width)
            height_node.text=str(height)
            for member in xml_root.findall('object'):
                member[4][0].text=str(int(int(member[4][0].text)*wpercent))
                member[4][1].text=str(int(int(member[4][1].text)*wpercent))
                member[4][2].text=str(int(int(member[4][2].text)*wpercent))
                member[4][3].text=str(int(int(member[4][3].text)*wpercent))
                value=(xml_root.find('filename').text,
                int(xml_root.find('size')[0].text),
                int(xml_root.find('size')[1].text),
                member[0].text,
                int(int(member[4][0].text)),
                int(int(member[4][1].text)),
                int(int(member[4][2].text)),
                int(int(member[4][3].text))
                )
                xml_data.append(value)
            xmlnode.write(os.path.join(OUTPUT_XML_PATH,xml_file))
             
column_name = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
xml_df = pd.DataFrame(xml_data, columns=column_name)
xml_df.to_csv(CSV_SAVE_PATH,index=None)

"""
Generate Data Summary and save in csv format

"""
a = xml_df['class'].value_counts()
df_summary = pd.DataFrame()
df_summary['classes'] = pd.Series(a.index)
df_summary['counts'] = pd.Series(a.data)
df_summary.to_csv(CSV_SUMMARY_SAVE_PATH,index=None)


"""
Check if an xml exist for any image anywhere inside root folder. If there exist
copy the image to images folder

"""
            
##Copying images 
for root,dirs,files in os.walk(SUB_IMAGES_PATH):
    for image_file in files:
        if(image_file.endswith('png') or image_file.endswith('PNG') or image_file.endswith('jpg')or image_file.endswith('JPG')):
#             print(image_file)
            s = image_file.split('.')
            file_name = s[0]
            for t in range(len(s)-2):
                file_name= file_name +'.'+ s[t+1]
            frt = s[-1]
            xml_file = os.path.join(OUTPUT_XML_PATH,file_name)+'.xml'
            if(os.path.exists(xml_file)):
                if not (os.path.exists(os.path.join(OUTPUT_IMAGES_PATH,image_file))): 
                    img = Image.open(os.path.join(root,image_file)).convert('RGB')
                    width = img.size[0]
                    height = img.size[1]
                    if (not width==0) or (not height==0):
                        if (width>height):
                            wpercent = (MAX_WIDTH/float(img.size[0]))
                            hsize = int((float(height)*float(wpercent)))
                            wsize = MAX_WIDTH
                        else:
                            wpercent = (MAX_HEIGHT/float(img.size[1]))
                            wsize = int((float(width)*float(wpercent)))
                            hsize = MAX_HEIGHT
                    else:
                        continue
                    if (wpercent<1):
                        img = img.resize((wsize,hsize), Image.ANTIALIAS)
                        img.save(os.path.join(OUTPUT_IMAGES_PATH,image_file))
                    else:
                        shutil.copy(os.path.join(root,image_file),os.path.join(OUTPUT_IMAGES_PATH,image_file))
                      
            
"""
Seqregate the TrainVal and Test data

"""


##Defining TrainVal and TestingSet
files_list = os.listdir(OUTPUT_XML_PATH)
files_count = len(files_list)
total_train = int(files_count * TRAIN_RATIO)
total_test = files_count - total_train

with open(TRAINVAL_FILE,'w') as f:
    for i in range(total_train):
        f.write(files_list[i].split('.')[0]+'\n')
        

with open(TEST_FILE,'w') as f:
    for i in range(total_test):
        f.write(files_list[i+total_train].split('.')[0]+'\n')


