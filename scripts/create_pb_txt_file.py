"""
File to convert lables.txt file to pbtxt format

"""

labels_file_path = './labels.txt'
output_pbtxt_path = './object_detection_labels.pbtxt'
with open(labels_file_path,'r') as f:
  labels = [l.rstrip() for l in f.readlines()]

with open(output_pbtxt_path,'w') as f:
    for i,l in enumerate(labels):
        f.write('item {\n  id: '+str(i+1)+'\n  name: '+'\''+l+'\''+'\n}\n\n')
        
    
