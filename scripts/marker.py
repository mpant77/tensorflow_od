import matplotlib.pyplot as plt
import numpy as np
import sys 

def calculate_area(x,y):
    area = 0
    def area_under_two_points(x1,y1,x2,y2):
        base_length = np.abs(x1-x2)
        return (0.5 * base_length * np.abs(y1-y2)) + base_length * np.abs(min(y1,y2))
    
    for i in range(len(x)-1):
        area = area + area_under_two_points(x[i],y[i],x[i+1],y[i+1])
    return area

np_path=sys.argv[1]
print(np_path)
category_name=np_path.split('.npy')[0].split("/")[-1]
rc_numpy = np.load(np_path)

x = rc_numpy[1]
y = rc_numpy[0]
#names = np.array(list("ABCDEFGHIJKLMNO"))
c = rc_numpy[0]

norm = plt.Normalize(1,4)
cmap = plt.cm.RdYlGn

fig,ax = plt.subplots()
sc = plt.scatter(x,y, s=100, cmap=cmap, norm=norm)
plt.xlabel('Recall')
plt.ylabel('Precision')
plt.title('Scatter: Precision Vs Recall Curve\n'+category_name)
annot = ax.annotate("", xy=(0,0), xytext=(20,20),textcoords="offset points",
                    bbox=dict(boxstyle="round", fc="w"),
                    arrowprops=dict(arrowstyle="->"))
annot.set_visible(False)
print('Area = {:0.2f}'.format(calculate_area(x,y)))

def update_annot(ind):

    pos = sc.get_offsets()[ind["ind"][0]]
    annot.xy = pos
    #text = "{}".format(" ".join(list(map(str,ind["ind"]))))
    text = "{:0.2f}".format(c[ind['ind'][-1]])
    annot.set_text(text)
    annot.get_bbox_patch().set_facecolor(cmap(norm(c[ind["ind"][0]])))
    annot.get_bbox_patch().set_alpha(0.4)


def hover(event):
    vis = annot.get_visible()
    if event.inaxes == ax:
        cont, ind = sc.contains(event)
        if cont:
            update_annot(ind)
            annot.set_visible(True)
            fig.canvas.draw_idle()
        else:
            if vis:
                annot.set_visible(False)
                fig.canvas.draw_idle()

fig.canvas.mpl_connect("motion_notify_event", hover)

plt.show()
