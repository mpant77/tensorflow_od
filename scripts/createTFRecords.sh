#!/bin/bash

export PYTHONPATH=`pwd`/..:`pwd`/../slim
DATA_DIRECTORY=/home/dsserver/projects/Manish/OD/processed-data
OUTPUT=$DATA_DIRECTORY/tf_records
LABEL_FILE=$DATA_DIRECTORY/object-detection.pbtxt	

mkdir $OUTPUT
echo $LABEL_FILE,$DATA_DIRECTORY,$OUTPUT
python -u  ../object_detection/dataset_tools/create_tf_record.py \
    --label_map_path=$LABEL_FILE \
    --data_dir=$DATA_DIRECTORY \
    --num_shards=64 \
    --output_dir=$OUTPUT >tf.txt 2>&1 
